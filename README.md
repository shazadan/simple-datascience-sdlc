Simple Data Science SDLC Demo
==============================


Description
-----------
A collection of files to showcase how to develop, build and deploy an application or service in python


Pre-requisites
--------------
1. Install [Anaconda](https://www.continuum.io/downloads).
2. Git
```
$ pip install git
```
3. Fabric
```
$ pip install fabric
```

Quick Start
-----------
Step 1: Clone this library to your local machine. 
```
$ git clone https://[bitbucket user id]@bitbucket.org/silverrockinc/simple-datascience-sdlc.git
```




