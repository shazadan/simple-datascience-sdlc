import win32service
import win32serviceutil
import win32event
import web
import server
import sys

class MyAppService(win32serviceutil.ServiceFramework):
    # you can NET START/STOP the service by the following name
    _svc_name_ = "myapp"
    # this text shows up as the service name in the Service
    # Control Manager (SCM)
    _svc_display_name_ = "MyApp Service"
    # this text shows up as the description in the SCM
    _svc_description_ = "MyApp Service"

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        # create an event to listen for stop requests on
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

    # core logic of the service
    def SvcDoRun(self):
    	# python code goes here
        import servicemanager
        server.run()

    # called when we're being shut down
    def SvcStop(self):
        # tell the SCM we're shutting down
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # fire the stop event
        win32event.SetEvent(self.hWaitStop)
        # stop the web server
        web.httpserver.server.interrupt = KeyboardInterrupt()

if __name__ == '__main__':
    sys.frozen = 'windows_exe'  # Fake py2exe so we can debug
    win32serviceutil.HandleCommandLine(MyAppService)