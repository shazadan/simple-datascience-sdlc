import web

urls = ("/.*", "hello")


class hello:
    def GET(self):
        return 'Hello Back, Imran! Thanks for setting up our deploy pipeline. @Cem - You are welcome!'

def run():
    app = web.application(urls, globals())
    http_server = web.httpserver.runsimple(app.wsgifunc(), ('0.0.0.0', 8786))
    http_server.run()

if __name__ == "__main__":
    run()